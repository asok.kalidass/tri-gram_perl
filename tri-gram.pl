#!/usr/bin/perl
# Course: CSCI 6509
# Author: Asok Kalidass K 
# Description: Solution tri-gram.pl (converts the text file into trigram and then search 
#                                for the given search word in middle in addition with 
#                                start &  end special cases)

use warnings;
use strict;
#Declarations
my @trigram_array;
my @matched_words;
my @ending_words;

#store the search word 
my $search_word = lc (shift);

#Read from file or command input
while (<>) {
    chomp($_);
    #preprocessing the data
    #Removing the special characters
    $_ =~ s/[`!,.?:]+//g;
    $_ =~ s/["']/ /ig;
    $_ =~ s/\s+$//;
    $_ = join (" ", @ending_words, $_); 
    
    @ending_words = join(" ", (split /\s/, $_)[-2 .. -1]);
    
    #Read the word from the line 
    foreach my $word (/(?=(\w+ \w+ \w+))\w+/g) { #(/(\w+(?:'\w+)*)/g) {              	    
	 push @trigram_array, lc ($word);                                                 	         
    }    
}

#Check if any of the start word is matching with the search word and print 
#them with the starting word as START. 
&print_output(shift @trigram_array);

#grep and pring the mathed pattern 
for ( grep { /.* $search_word .*/i } @trigram_array ) {
       my @word = split (/\s/, $_);
       printf ("%12s %s %s\n", $word[0], $word[1], $word[2]);
       push @matched_words, $_; 
}

#Check if any of the end word is matching with the search word and print 
#them with the ending word as END.

&print_output(pop @trigram_array);

#sub routine to handle the start and end of the matching word in the text.
sub print_output() {
  my @word = split (/\s/, $_[0]);
 # print "***@_***\n";
  if ($word[0] eq lc $search_word) {
  	printf ("%12s %s %s\n", "START", $word[0], $word[1]);
  }
  else {
	 if ($word[2] eq lc $search_word) {
     	      printf ("%12s %s %s\n", $word[1], $word[2], "END");
         }
      }
}
