Pre-requisite 

Perl

Sample Input :

‘‘Tom, you didn’t have to undo your shirt collar where I sewed it, to
pump on your head, did you? Unbutton your jacket!’’
The trouble vanished out of Tom’s face. He opened his jacket. His shirt
collar was securely sewed.
‘‘Bother! Well, go ’long with you. I’d made sure you’d played hookey and
been a-swimming. But I forgive ye, Tom. I reckon you’re a kind of a
singed cat, as the saying is -- better’n you look. THIS time.’’
She was half sorry her sagacity had miscarried, and half glad that Tom
had stumbled into obedient conduct for once.
But Sidney said:
‘‘Well, now, if I didn’t think you sewed his collar with white thread,
but it’s black.’’
‘‘Why, I did sew it with white! Tom!’’
But Tom did not wait for the rest. As he went out at the door he said:
‘‘Siddy, I’ll lick you for that.’’


Output:

./tri-gram.pl his a2q2-test1.in (command to execute the program in terminal/unix)
the obtained output must be:
opened his jacket
jacket his shirt
sewed his collar

after running: ./tri-gram.pl TOM a2q2-test1.in
the output must be:
START tom you
of tom s
ye tom i
that tom had
white tom but
but tom did
and after running: ./tri-gram.pl that a2q2-test1.in
the output must be:
glad that tom
for that END